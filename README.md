# LAMP Automation

## Setup
This setup is for use with an AWS account. All resources fall within the free tier but cost could be incurred.

### Prerequisites

- A service role for Cloudformation with the ability to deploy all components. In practice, this is almost everything so a broadly permissive role with a trust relationship with cloudformation.amazonaws.com works.
- A user for Gitlab with permissions to S3, CodeDeploy and Cloudformation, and the ability to pass role to the cloudformation service role.

### Clone This Repository

```git clone git@gitlab.com:bdlj/lamp-automation.git```

### Create a Gitlab Repo
Create an empty repository on Gitlab, this will be used to push to in a moment.

### Gitlab CI/CD Variables
- `AWS_ACCESS_KEY` : Keys for the Gitlab user
- `AWS_SECRET_KEY`  ^^
- `AWS_DEFAULT_REGION` : This is designed to run specifically in us-east-1
- `CLOUDFORMATION_ROLE_ARN` : Service role for CloudFormation
- `STACK_PREFIX` : This is a common identifier for all resources within this project. Should be a short alphanumeric identifier, with dashes allowed. An important note - this controls the name of an s3 bucket that will be used for storing artifacts. As s3 buckets must have globally unique names across _all_ AWS accounts, care should be taken to ensure this stack identifier is unique enough.

### Add New Repository as Remote and Push

1. Get the repository url from the repository page on Gitlab
2. Add the new respotiroy as the origin
```
git remote remove origin
git remote add origin <new_repository_url>
git push origin main
```

This should trigger a build on the newly created repository and build all components inside the AWS account provided.

### Verify
After the pipeline completes, find the public IP of the created instance and visit it at `<public_ip>:80/`, this should display a message including the deployed commit long hash.

Running instances can be accessed via the AWS SSM Session Manager, available either in the console or via the Session Manager plugin.

## Components
- A VPC with an Internet Gateway and two public subnets
- An Autoscaling Group across both public subnets
- A launch configuration that bootstraps an instance with required packages
- A CodeDeploy application and deployment group targeted at the instances from the created autoscaling group that handles artifact updates on the running instance(s)

## Possible Improvements
- Very little conditionals, improved change detection could lead to less running on each push to main.
- Very little error handling, pipeline will attempt to continue even if individual pieces report failure.
- All services are colocated on a single instance. This is contrived, and everything should be split to provide separation of concerns and reduce the blast radius of any compromised components.
- App installations are generally insecure, the bare minimum of tweaks have been made.
- IAM roles used are overly permissive.