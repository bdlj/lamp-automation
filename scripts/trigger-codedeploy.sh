#!/bin/bash

# Infer codedeploy application and trigger a deployment
STACK_PREFIX=$1


deployment_id=$(aws deploy create-deployment --application-name lamp-automation \
--deployment-config-name CodeDeployDefault.AllAtOnce \
--deployment-group-name lamp-automation-group \
--s3-location bucket=${STACK_PREFIX}-artifactbucket,bundleType=tgz,key=bundle.tar.gz)