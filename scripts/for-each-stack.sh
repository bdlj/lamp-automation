#!/bin/bash

# Loops through all files in templates/ and deploys them with the specified STACK_PREFIX

STACK_PREFIX="$1"
ROLE_ARN="$2"

for TEMPLATE in templates/*; do
    STACK_NAME="${STACK_PREFIX}-$(echo $TEMPLATE | cut -d. -f1 | cut -d/ -f2)"
    ./scripts/create-or-update-stack.sh "$STACK_NAME" "${TEMPLATE}"
done