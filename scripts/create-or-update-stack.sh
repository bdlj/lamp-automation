#!/bin/bash
set -x
# Checks for an existing stack and either creates or updates the existing one.
# Stackname Filename JSON Parameters RoleArn

CREATE_STACK="false"

while getopts ":s:f:r:p:" opt; do
  case $opt in
    s)
      STACK_NAME=$OPTARG
      ;;
    f)
      FILE_NAME=$OPTARG
      ;;
    r)
      ROLE_ARN=$OPTARG
      ;;
    p)
      PARAMETERS=$OPTARG
      ;;    
    \?)
      echo "Invalid option: -$OPTARG"
      ;;
    :)
      echo "Option -$OPTARG requires an argument."
      exit 1
      ;;
  esac
done

if ! aws cloudformation describe-stacks --stack-name "${STACK_NAME}"; then
#if [[ $? -ne 0 ]]; then
    CREATE_STACK="true"
fi
if [[ $CREATE_STACK == "true" ]]; then
    if [[ -z $PARAMETERS ]]; then
        aws cloudformation create-stack --template-body "file://${FILE_NAME}" --capabilities CAPABILITY_IAM --role-arn "${ROLE_ARN}" --stack-name "${STACK_NAME}"
        aws cloudformation wait stack-create-complete --stack-name "${STACK_NAME}"
    else
        aws cloudformation create-stack --template-body "file://${FILE_NAME}" --capabilities CAPABILITY_IAM --role-arn "${ROLE_ARN}" --stack-name "${STACK_NAME}" --parameters "${PARAMETERS}"
        aws cloudformation wait stack-create-complete --stack-name "${STACK_NAME}"
    fi
else
    if [[ -z $PARAMETERS ]]; then
          if aws cloudformation update-stack --template-body "file://${FILE_NAME}" --capabilities CAPABILITY_IAM --stack-name "${STACK_NAME}"; then
            aws cloudformation wait stack-update-complete --stack-name "${STACK_NAME}"
          fi
    else
        if aws cloudformation update-stack --template-body "file://${FILE_NAME}" --capabilities CAPABILITY_IAM --stack-name "${STACK_NAME}" --parameters ${PARAMETERS}; then
        aws cloudformation wait stack-update-complete --stack-name "${STACK_NAME}"
        fi
    fi
fi