# Bundles files in resources/



(
    cd resources/
    sed -i s/COMMIT_HASH/${CI_COMMIT_SHA}/ var/www/html/index.php
    tar -czvf ../bundle.tar.gz *
)