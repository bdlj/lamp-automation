#!/bin/bash


STACK_PREFIX=$1
ARTIFACT_BUCKET="${STACK_PREFIX}-artifactbucket"
# mb can attempt to create existing buckets without issue
aws s3 mb "s3://${ARTIFACT_BUCKET}"

# upload zip to bundle
aws s3 cp bundle.tar.gz "s3://${ARTIFACT_BUCKET}/bundle.tar.gz"

# upload userdata init script
aws s3 cp scripts/userdata.sh "s3://${ARTIFACT_BUCKET}/userdata.sh"