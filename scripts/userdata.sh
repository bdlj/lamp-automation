# Install dependencies

# Pull bundle and chown to httpd

# Switch user to httpd

# tar --no-overwrite-dir -xzvf bundle.tar.gz -C /

yum install -y ruby httpd mariadb-server ruby wget
amazon-linux-extras install -y lamp-mariadb10.2-php7.2 php7.2

wget https://aws-codedeploy-us-east-1.s3.us-east-1.amazonaws.com/latest/install && chmod +x ./install && ./install auto

service httpd start
service mariadb start